package bootstrap

import java.io.File
import javax.inject.Inject

import dao.{AirportDao, CountryDao, RunwayDao}
import models.{Airport, Country, Runway}
import play.core.Paths
import purecsv.unsafe.CSVReader
import services.QueryService

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.Duration
import scala.util.Try

private[bootstrap] class InitialData @Inject()(queryService: QueryService)(implicit ec:ExecutionContext) {

  def insert():Unit = {
    val insertData = for{
      count <- queryService.getCountryCount() if count == 0
      _ <- queryService.insert(InitialData.runways)
      _ <- queryService.insert(InitialData.countries)
      _ <- queryService.insert(InitialData.airports)

    } yield()
    Try(Await.result(insertData, Duration.Inf))
  }

  insert()
}

private[bootstrap] object InitialData{

  def countries = {
    CSVReader[Country].readCSVFromFile(new File(this.getClass.getResource("/resources/countries.csv").getPath()), skipHeader = true).toSeq
  }

  def airports = {
    CSVReader[Airport].readCSVFromFile(new File(this.getClass.getResource("/resources/airports.csv").getPath()), skipHeader = true).toSeq
  }
  def runways = {
    CSVReader[Runway].readCSVFromFile(new File(this.getClass.getResource("/resources/runways.csv").getPath()), skipHeader = true).toSeq
  }
}
