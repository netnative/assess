package dao

import javax.inject.{Inject, Singleton}

import models.Airport
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import scala.concurrent.{ExecutionContext, Future}

trait AirportsComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  class AirportsTable(tag: Tag) extends Table[Airport](tag, "AIRPORT") {
    def id = column[Long]("ID", O.PrimaryKey)
    def ident = column[String]("IDENT")
    def airportType = column[String]("AIRPORTTYPE")
    def name = column[String]("NAME")
    def latitudeDeg = column[Double]("LATITUDEDEG")
    def longitudeDeg = column[Double]("LONGITUDEDEG")
    def elevationFt = column[Option[Int]]("ELEVATIONFT")
    def continent = column[String]("CONTINENT")
    def isoCountry = column[String]("ISOCOUNTRY")
    def isoRegion = column[String]("ISOREGION")
    def municipality = column[String]("MUNICIPALITY")
    def scheduledService = column[String]("SCHEDULEDSERVICE")
    def gpsCode = column[Option[String]]("GPSCODE")
    def iataCode = column[Option[String]]("IATACODE")
    def localCode = column[Option[String]]("LOCALCODE")
    def homeLink = column[Option[String]]("HOMELINK")
    def wikipediaLink = column[Option[String]]("WIKIPEDIALINK")
    def keywords = column[Option[String]]("KEYWORDS")

    override def * = (id, ident, airportType, name, latitudeDeg, longitudeDeg, elevationFt, continent,
    isoCountry, isoRegion, municipality, scheduledService, gpsCode, iataCode, localCode, homeLink, wikipediaLink, keywords) <> (Airport.tupled, Airport.unapply)
  }
}

@Singleton
class AirportSlickDao @Inject()(protected val dbConfigProvider:DatabaseConfigProvider)(implicit ec:ExecutionContext)
  extends AirportsComponent with AirportDao with HasDatabaseConfigProvider[JdbcProfile]{

  import profile.api._

  val airports = TableQuery[AirportsTable]

  /**
    * This method is used only for filling database with test data
    * @param airports
    * @return Nothing
    */
  def insert(airports:Seq[Airport]): Future[Unit] = {
    db.run(this.airports ++= airports).map(_ => ())
  }
  /**
    * This is only for filling test data
    * @return Nothing
    */
  def count():Future[Int] = {
    db.run(airports.length.result)
  }
}
