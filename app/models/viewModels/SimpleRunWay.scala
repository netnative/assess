package models.viewModels

import play.api.libs.json.Json

case class SimpleRunWay(id:Long, lengthFt:Option[Int], widthFt:Option[Int], surface:Option[String])

object SimpleRunWay{
  implicit val simpleRunwayFormat = Json.format[SimpleRunWay]
}
