if (window.console) {
  console.log("Welcome to your Play application's JavaScript!");
}
// load when document get ready
$(document).ready(function(){

    // this is a click handler for search the countries based on their name or code
    $("#btnSearch").click(function(){
        var filter = $("#txtfilter").val();
        if(filter.length > 0)
            getCountries(filter);
        else
            showError();
    });
    // this is a click handler to show runways of each airport
    $(document).on('click', '.btnRunway', function(){
        var airportId = $(this).prop("id");
        console.log(airportId);
        getRunways(airportId);
    });
    $( document ).ajaxStart(function() {
        console.log("ajax started")
        $.LoadingOverlay("show");
    });
    $( document ).ajaxStop(function() {
        console.log("ajax stoped")
        $.LoadingOverlay("hide");
    });
    $("#btnReportShow").click(function(){

        fillMostLeastAirports();
        fillCountrySurface();
        fillMostCommonLeIdens();

    })
});
function showError() {
    var content = '<div class="col text-danger">Please enter country name or code</div>';
    $("#listCountyContainer").html(content);
}
function fillMostCommonLeIdens() {
    getMostCommonLeIdent(10, $("#listLeIdens"));
}
function getMostCommonLeIdent(count, container) {
    var url = jsRoutes.controllers.HomeController.getMostCommonIdent(count).url;
    $.ajax({
        url:url,
        type:'get',
        contentType: "application/json; charset=utf-8",
    }).done(function(results){
        console.log(results);
        makeMostCommonLeIdentRows(results, container)
    });
}
function makeMostCommonLeIdentRows(rows, container) {
    var content = "";
    $.each(rows, function (k, v) {
        content += '<div class="row">' +
                        '<div class="col">' + v + '</div>' +
                    '</div>';
    });
    container.html(content);
}
function fillCountrySurface(){
    getCountySurface(10, $("#listSurface"));
}
function getCountySurface(count, container) {
    var url = jsRoutes.controllers.HomeController.getCityAirportRunwaySurface(count).url;
    $.ajax({
        url:url,
        type:'get',
        contentType: "application/json; charset=utf-8",
    }).done(function(results){
        console.log(results);
        makeCountrySurfaceRows(results, container)
    });
}
function makeCountrySurfaceRows(rows, container) {
    var content = "";
    $.each(rows, function (k, v) {
        content += '<div class="surfaceRows" name="surfaceRows">' +
                        '<div class="col">' + v[0] + '</div>' +
                    '</div>';
        $.each(v[1], function (k1, v1) {
            content += '<div class="row">' +
                            '<div class="col offset-1">' + v1 + '</div>' +
                        '</div>';
        });
    });
    container.html(content);
}
function fillMostLeastAirports() {
    getCountrisWithMostLessAirports("asc", 10, $("#topleastAirport"));
    getCountrisWithMostLessAirports("desc", 10, $("#topMostAirport"))
}
function getCountrisWithMostLessAirports(order, count, container) {
    var url = jsRoutes.controllers.HomeController.getTopDownCountires(order, count).url;
    $.ajax({
        url:url,
        type:'get',
        contentType: "application/json; charset=utf-8",
    }).done(function(results){
        makeMostLeastRows(results, container)
    });
}

function makeMostLeastRows(rows, container) {
    var content = "";
    $.each(rows, function (k, v) {
        content += '<div class="row">' +
            '<div class="col-2">' + v.code + '</div>' +
            '<div class="col-8">' + v.name + '</div>' +
            '<div class="col-2">' + v.continent + '</div>' +
            '</div>';
    });
    container.html(content);
}

function getCountries(filter){
  var url = jsRoutes.controllers.HomeController.getCountries(filter).url;
  $.ajax({
     url:url,
     type:'get',
      contentType: "application/json; charset=utf-8",
  }).done(function(results){
    console.log(results);
    makeRows(results);

  });
}

function makeRows(rows) {
    var content = '<div id="lblColumn" class="col">';
    $.each(rows, function (k, v) {
        content += '<div class="row">' + '<h2>' + v.simpleCountryView.name + ':<small class="text-muted">' + v.simpleCountryView.continent + '</small>' +  '</h2></div>';
        content += '<div class="row"><div class="col">' +
            '<div class="row">' +
            '<div class="col-3 bg-secondary">Name</div>' +
            '<div class="col-2 bg-secondary">Airport Type</div>' +
            '<div class="col-2 bg-secondary">Latitude Deg</div>' +
            '<div class="col-2 bg-secondary">Runways</div>' +
            '</div>';
        $.each(v.airports, function (airKey, airVal) {
            content += '<div class="row">' +
                            '<div class="col">' +
                                '<div class="row">' +
                                    '<div class="col-3">' + airVal.name + '</div>' +
                                    '<div class="col-2">' + airVal.airportType + '</div>' +
                                    '<div class="col-2">' + airVal.latitudeDeg + '</div>' +
                                    '<div class="col-2"><a class="btnRunway badge badge-light" id="' + airVal.ident + '">runways</a></div>' +
                                '</div>'+
                                '<div class="row" id="listRunway' + airVal.ident + '"></div>' +
                            '</div>' +
                        '</div>';
        });
        content += '</div>';
        content += '</div>';
    });
    $("#listCountyContainer").html(content);
}
function getRunways(airportId) {
        var data = JSON.stringify({id: airportId});
        var url = jsRoutes.controllers.HomeController.getRunways(airportId).url;
        $.ajax({
            url: url,
            type: 'get',
            contentType: "application/json; charset=utf-8"
        }).done(function (results) {
            console.log(results);
            makeRunwayRows(results, airportId);
        });
    }

function makeRunwayRows(rows, airportId) {
    var content = '<div class="col offset-1">';
    if (rows.length == 0){
        content += '<div class="row">' +
                        '<div class="col text-danger">' + 'This airport does not has any runway' + '</div>' +
                    '</div>';
    }else {
        content += '<div class="row">' +
            '<div class="col-2 bg-secondary">surface</div>' +
            '<div class="col-1 bg-secondary">LenghtFT</div>' +
            '<div class="col-1 bg-secondary">widthFT</div>' +
            '</div>';
        $.each(rows, function (k, v) {
            content += '<div class="row">' +
                '<div class="col-2">' + v.surface + '</div>' +
                '<div class="col-1">' + v.lengthFt + '</div>' +
                '<div class="col-1">' + v.widthFt + '</div>' +
                '</div>';
        });
    }
    content += '</div>';
    $("#listRunway" + airportId).html(content);
    $("#listRunway" + airportId).show();
}