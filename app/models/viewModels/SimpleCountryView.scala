package models.viewModels

import play.api.libs.json.{Format, Json}

case class SimpleCountryView(id:Long, name:String, code:String, continent:String, wikipediaLink:String, keywords:Option[String])

object SimpleCountryView{
  implicit val simpleCountryViewFormat = Json.format[SimpleCountryView]
  val tupled = (this.apply _).tupled
}
