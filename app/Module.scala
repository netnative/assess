import com.google.inject.AbstractModule
import java.time.Clock

import dao._
import services.{QueryService, QueryServiceImpl}
/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.

 * Play will automatically use any class called `Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */
class Module extends AbstractModule {

  override def configure() = {

    bind(classOf[RunwayDao]).to(classOf[RunwaySlickDao]).asEagerSingleton()
    bind(classOf[CountryDao]).to(classOf[CountrySlickDao]).asEagerSingleton()
    bind(classOf[AirportDao]).to(classOf[AirportSlickDao]).asEagerSingleton()
    bind(classOf[QueryService]).to(classOf[QueryServiceImpl]).asEagerSingleton()
  }

}
