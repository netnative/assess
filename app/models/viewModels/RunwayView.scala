package models.viewModels

case class RunwayView(id:Long, lengthFt:Option[Int], widthFt:Option[Int], surface:Option[String])
