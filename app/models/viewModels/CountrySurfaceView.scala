package models.viewModels

import play.api.libs.json.Json

case class CountrySurfaceView(name:String, surfaces:Seq[String])

object CountrySurfaceView{
  implicit val countrySurfaceFormat = Json.format[CountrySurfaceView]
  val tupled = (this.apply _).tupled
}