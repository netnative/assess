package dao

import javax.inject.{Inject, Singleton}

import models.Runway
import models.viewModels.SimpleRunWay
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import slick.model.Column

import scala.concurrent.{ExecutionContext, Future}

trait RunwayComponent{
 self:HasDatabaseConfigProvider[JdbcProfile] =>
  import profile.api._

  class RunwaysTable(tag:Tag) extends Table[Runway](tag, "RUNWAY"){
    def id = column[Long]("ID", O.PrimaryKey)
    def airportRef = column[Long]("AIRPORTREF")
    def airportIdent = column[String]("AIRPORTIDENT")
    def lengthFt = column[Option[Int]]("LENGTHFT")
    def widthFt = column[Option[Int]]("WIDTHFT")
    def surface = column[Option[String]]("SURFACE")
    def lighted = column[Boolean]("LIGHTED")
    def closed = column[Boolean]("CLOSED")
    def leIdent = column[Option[String]]("LEIDENT")
    def leLatitudeDeg = column[Option[Double]]("LELATITUDEDEG")
    def leLongitudeDeg = column[Option[Double]]("LELONGITUDEDEG")
    def leElevationFt = column[Option[Int]]("LEELEVATIONFT")
    def leHeadingDegT = column[Option[Double]]("LEHEADINGDEGT")
    def leDisplacedThresholdFt = column[Option[Int]]("LEDISPLACEDTHRESHOLDFT")
    def heIdent = column[Option[String]]("HEIDENT")
    def heLatitudeDeg = column[Option[Double]]("HELATIITUDEDEG")
    def heLongitudeDeg = column[Option[Double]]("HELONGITUDEDEG")
    def heElevationFt = column[Option[Int]]("HEELEVATIONFT")
    def heHeadingDegT = column[Option[Double]]("HEHEADINGDEGT")
    def heDisplacedThresholdFt = column[Option[Int]]("HEDISPLACEDTHRESHOLDFT")

    override def * = (id,	airportRef,	airportIdent, lengthFt, widthFt,	surface,
      lighted, closed, 	leIdent, leLatitudeDeg, leLongitudeDeg,
      leElevationFt,	leHeadingDegT, leDisplacedThresholdFt, heIdent, heLatitudeDeg,
      heLongitudeDeg,	heElevationFt, heHeadingDegT,	heDisplacedThresholdFt) <> (Runway.tupled, Runway.unapply)
  }
}

@Singleton
class RunwaySlickDao @Inject()(protected val dbConfigProvider:DatabaseConfigProvider)(implicit ec:ExecutionContext) extends RunwayComponent with RunwayDao with HasDatabaseConfigProvider[JdbcProfile]{

  import profile.api._

  val runways = TableQuery[RunwaysTable]

  /**
    * This method is used only for filling database with test data
    * @param runways
    * @return Nothing
    */
  def insert(runways:Seq[Runway]):Future[Unit] = {
    db.run(this.runways ++= runways).map(_ => ())
  }

  def count():Future[Int] = {
    db.run(runways.length.result)
  }
  /**
    * Get a airport ident and returns its runways
    * @param ident
    * @return Future[Seq[SimpleRunWay]] : It contains sequence of runways. We only returns some fields
    */
  def getByAirportIdent(ident:String):Future[Seq[SimpleRunWay]] = {
    val q = runways.filter(_.airportIdent === ident).result
    q.statements.foreach(println)
    db.run(q).map {
      case r => r.map { runway => SimpleRunWay(runway.id, runway.lengthFt, runway.widthFt, runway.surface)
      }
    }
  }

  /**
    * Returns most common runways based on the leIdent filed
    * @param
    * @return Future[Seq[Option[String]]] : Sequence of runways name
    */
  def getMostCommonRunways(count:Int) : Future[Seq[Option[String]]] = {
    val q = runways.groupBy(_.leIdent).map{
      case (leIdent, runs) => (leIdent, runs.length)
    }.sortBy(_._2.desc).take(count).map(_._1)
    db.run(q.result)
  }
}
