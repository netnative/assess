package bootstrap

import com.google.inject.AbstractModule

class DBModule extends AbstractModule{
  override protected def configure(): Unit = {
    bind(classOf[InitialData]).asEagerSingleton()
  }
}
