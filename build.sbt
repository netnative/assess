name := "assessment"
 
version := "1.0" 
      
lazy val `assessment` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
      
scalaVersion := "2.12.2"

libraryDependencies ++= Seq( ehcache , ws , specs2 % Test , guice )
libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-slick" % "3.0.2",
  "com.typesafe.play" %% "play-slick-evolutions" % "3.0.2",
  "com.microsoft.sqlserver" % "mssql-jdbc" % "6.2.1.jre8",
  "com.h2database" % "h2" % "1.4.192",
  "com.github.tototoshi" %% "scala-csv" % "1.3.5",
  "com.github.melrief" %% "purecsv" % "0.1.1"
)

unmanagedResourceDirectories in Test +=  baseDirectory.value / "target/web/public/test"


      