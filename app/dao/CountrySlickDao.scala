package dao

import javax.inject.{Inject, Singleton}

import models.Country
import models.viewModels.{AirportView, CountryView, SimpleCountryView}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}


trait CountriesComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  class CountriesTable(tag: Tag) extends Table[Country](tag, "COUNTRY") {
    def id = column[Long]("ID", O.PrimaryKey)

    def name = column[String]("NAME")
    def code = column[String]("CODE")
    def continent = column[String]("CONTINENT")
    def wikipediaLink = column[String]("WIKIPEDIALINK")
    def keywords = column[Option[String]]("KEYWORDS")

    override def * = (id, code, name, continent, wikipediaLink, keywords) <> (Country.tupled, Country.unapply)
  }
}

@Singleton
class CountrySlickDao @Inject()(protected val dbConfigProvider:DatabaseConfigProvider)(implicit ec:ExecutionContext)
extends CountriesComponent with AirportsComponent with RunwayComponent with CountryDao with HasDatabaseConfigProvider[JdbcProfile]{

  import profile.api._

  val countries = TableQuery[CountriesTable]
  val airports = TableQuery[AirportsTable]
  val runways = TableQuery[RunwaysTable]

  /**
    * This method is used only for filling database with test data
    * @param countries
    * @return Nothing
    */
  def insert(countries:Seq[Country]): Future[Unit] = {
    db.run(this.countries ++= countries).map(_ => ())
  }

  /**
    * This is only for filling test data
    * @return Nothing
    */
  def count():Future[Int] = {
    db.run(countries.length.result)
  }
  /**
    * Returns all of the cities
    * @return Nothing
    */
  def all():Future[Seq[SimpleCountryView]] = {
    db.run(countries.result).map {
      r =>
        r.map {
          c => SimpleCountryView(c.id, c.name, c.code, c.continent, c.wikipediaLink, c.keywords)
        }
    }
  }

  /**
    * This returns all the countirs and their airports which country has a specific name or code.
    * @param name : name is partial and ignore case for name column and is strict for code
    * @return Seq of CountryView model
    */
  def getByName(name:String):Future[Seq[CountryView]] = {

    val q = countries.filter(c => c.name.toLowerCase.like(name.toLowerCase() + "%") || c.code === name).take(5).
      join(airports).on(_.code === _.isoCountry)

    db.run(q.result).map{ a =>
      a.groupBy(_._1.id).map{
        case(_, tuples) =>
          val (country, _) = tuples.head
          val airports = tuples.map{case (_, air) => AirportView(air.id, air.ident, air.airportType, air.name, air.latitudeDeg)}
          CountryView(
            SimpleCountryView(country.id, country.name, country.code, country.continent, country.wikipediaLink, country.keywords),
            airports
          )
      }.toSeq
    }
  }

  /**
    * Returns the countries with most and less airports count
    * @param order : "asc" get less and "desc" get most
    * @param count : To set the count of return countirs
    * @return Future[Seq[SimpleCountryView]]
    */
  def getTopDownCountries(order:String, count:Int) : Future[Seq[SimpleCountryView]] = {
    val q = airports.groupBy(_.isoCountry).map{
      case (id, airs) => (id, airs.length)
    }.join(countries).on(_._1 === _.code).sortBy(order match {
      case "asc" => (_._1._2.asc)
      case "desc" => (_._1._2.desc)
      case _ => (_._1._2.desc)
    })

    db.run(q.take(count).map {
      case ((_, _), c) => (c)
    }.result).map{
      rows => rows.map {c =>
        SimpleCountryView(c.id, c.name, c.code, c.continent, c.wikipediaLink, c.keywords)
      }
    }
  }

  def getCountriesSurface(count:Int):Future[Seq[(String, Seq[Option[String]])]] = {
    val q = countries.take(count).join(airports).on(_.code === _.isoCountry).
      join(runways).on(_._2.ident === _.airportIdent)
      .groupBy {
        case ((c, _), r) => (c.name, r.surface)
      }.map {
      case ((name, surface), _) => (name, surface)
    }

    q.result.statements.foreach(println)

    db.run(q.result).map {
      rows =>
        rows.groupBy(_._1).map {
          case (c, tuples) => (c, tuples.map {
            case (x, y) => y
          })
        }.toSeq
    }

  }
}
