import dao.{AirportDao, CountryDao, RunwayDao}
import org.specs2.mutable.Specification
import play.api.Application
import play.api.test.WithApplication

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class DaoSpec extends Specification{

  "Country Dao" should {

    def countryDao(implicit app: Application) = {
      val app2DAO = Application.instanceCache[CountryDao]
      app2DAO(app)
    }
    def airportDao(implicit app: Application) = {
      val app2DAO = Application.instanceCache[AirportDao]
      app2DAO(app)
    }
    def runwayDao(implicit app: Application) = {
      val app2DAO = Application.instanceCache[RunwayDao]
      app2DAO(app)
    }

    "country has 247 record" in new WithApplication() {
      val result = Await.result(countryDao.count(), Duration.Inf)
      result must equalTo(247)
    }
    "airport has 46336 record" in new WithApplication() {
      val result = Await.result(airportDao.count(), Duration.Inf)
      result must equalTo(46336)
    }
    "runways has 39536 record" in new WithApplication() {
      val result = Await.result(runwayDao.count(), Duration.Inf)
      result must equalTo(39536)
    }
    "return 135 when country call getByName with iran as parameter " in new WithApplication() {
      val result = Await.result(countryDao.getByName("Iran"), Duration.Inf)
      result.length must equalTo(1)
    }
    "return data contains US in getTopDownCountries with 'DESC' and 10" in new WithApplication() {
      val result = Await.result(countryDao.getTopDownCountries("Desc", 10), Duration.Inf)
      result.head.code must contain("US")
    }

    "contains Angola and ASp on getCountrySurface" in new WithApplication{
      val result = Await.result(countryDao.getCountriesSurface(10), Duration.Inf)
      result.find(_._1 == "Angola").get._2 contains(Some("ASP")) must beTrue
    }
    "contains Airport mehraabad on getByAirportIdent" in new WithApplication{
      val result = Await.result(runwayDao.getByAirportIdent("OIII"), Duration.Inf)
      result.filter(_.surface.get == "CON") must have size(1)
    }
    "getMostCommonRunways contains H1 as LeIden" in new WithApplication() {
      val result = Await.result(runwayDao.getMostCommonRunways(10), Duration.Inf);
      result.filter(_ == Some("H1")) must have size(1)
    }

  }
}
