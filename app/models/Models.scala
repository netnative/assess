package models

sealed trait BaseModel

case class Country(id:Long, code:String, name:String,
                   continent:String, wikipediaLink:String, keywords:Option[String]) extends BaseModel

case class Airport (id:Long, ident:String, airportType:String, name:String,
                    latitudeDeg:Double, longitudeDeg:Double, elevationFt:Option[Int], continent:String,
                    isoCountry:String, isoRegion:String, municipality:String, scheduledService:String,
                    gpsCode:Option[String], iataCode:Option[String], localCode:Option[String], homeLink:Option[String],
                    wikipediaLink:Option[String], keywords:Option[String]) extends BaseModel

case class Runway(id:Long,	airportRef:Long,	airportIdent:String, lengthFt:Option[Int], widthFt:Option[Int],
                  surface:Option[String], lighted:Boolean, closed:Boolean, 	leIdent:Option[String],
                  leLatitudeDeg:Option[Double], leLongitudeDeg:Option[Double],
                  leElevationFt:Option[Int],	leHeadingDegT:Option[Double], leDisplacedThresholdFt:Option[Int],
                  heIdent:Option[String], heLatitudeDeg:Option[Double],
                  heLongitudeDeg:Option[Double],	heElevationFt:Option[Int], heHeadingDegT:Option[Double],
                  heDisplacedThresholdFt:Option[Int]) extends BaseModel