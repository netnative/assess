package models.viewModels

import play.api.libs.json.Json

case class AirportView(id:Long, ident:String, airportType:String, name:String, latitudeDeg:Double)

object AirportView{
  implicit val airportViewFormat = Json.format[AirportView]
  val tupled = (this.apply _).tupled
}
