package dao

import models.Airport

import scala.concurrent.Future

trait AirportDao {
  def insert(airports:Seq[Airport]): Future[Unit]
  def count():Future[Int]
}
