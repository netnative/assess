package services

import javax.inject.{Inject, Singleton}

import dao.{AirportDao, CountryDao, RunwayDao}
import models.viewModels.{CountryView, SimpleCountryView, SimpleRunWay}
import models.{Airport, Country, Runway}
import shapeless.TypeCase

import scala.concurrent.Future


@Singleton
class QueryServiceImpl @Inject()(countryDao: CountryDao, runwayDao: RunwayDao,

                                 airportDao: AirportDao) extends QueryService {

  override def getAirportCount() = airportDao.count()

  override def getCountryCount(): Future[Int] = countryDao.count()

  override def getRunwayCount(): Future[Int] = runwayDao.count()

  override def insert[BaseModel](data: Seq[BaseModel]) = {
    val countrySeq = TypeCase[Seq[Country]]
    val airportSeq = TypeCase[Seq[Airport]]
    val runwaysSeq = TypeCase[Seq[Runway]]

    data match {
      case countrySeq(data) => countryDao.insert(data)
      case airportSeq(data) => airportDao.insert(data)
      case runwaysSeq(data) => runwayDao.insert(data)
    }
  }
  override def getMostAndLeastCountries(order: String, count: Int): Future[Seq[SimpleCountryView]] = countryDao.getTopDownCountries(order, count)

  override def getMostCommonLeIdenInRunways(count: Int): Future[Seq[Option[String]]] = runwayDao.getMostCommonRunways(count)

  override def getRunwaysByAirportIden(airportIden: String): Future[Seq[SimpleRunWay]] = runwayDao.getByAirportIdent(airportIden)

  override def getSurfaceForEachCountry(count: Int): Future[Seq[(String, Seq[Option[String]])]] = countryDao.getCountriesSurface(count)

  override def getCountriesByName(filter: String): Future[Seq[CountryView]] = {
    countryDao.getByName(filter)
  }

}
