package models.viewModels

import play.api.libs.json.{Format, Json}

case class CountryView(simpleCountryView: SimpleCountryView, airports:Seq[AirportView])

object CountryView{
  implicit val CountryViewFormat = Json.format[CountryView]
  val tupled = (this.apply _).tupled
}
