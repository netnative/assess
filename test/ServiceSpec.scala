import dao.{AirportSlickDao, CountrySlickDao, RunwaySlickDao}
import models.{Airport, Country, Runway}
import org.specs2.mutable.Specification
import org.specs2.mock.Mockito
import play.api.test.WithApplication
import services.QueryServiceImpl

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

class ServiceSpec extends Specification with Mockito{

  val countries = Seq(Country(1L, "IR", "Iran", "AS", "", Some("data")))
  val airports = Seq(Airport(1L, "Semnan", "big", "Semnan Airport", 23.98232323, 32.232323,Some(10),"AS","IR","SD", "sdfs",
  "no", Some("sdfds"), None, None, None, None, None))
  val runways = Seq(Runway(1L, 1L, "Semnan",None, None, Some("H1"), true, false, Some("AH"), None, None, None,
    None, None, None, None, None, None, None, None))

  val countryMockDao = mock[CountrySlickDao]
  val airportMockDao = mock[AirportSlickDao]
  val runwayMockDao = mock[RunwaySlickDao]
  val queryService = new QueryServiceImpl(countryMockDao, runwayMockDao, airportMockDao)
  implicit val ex:ExecutionContext = ExecutionContext.global

  "QueryService" should {
    "return count = 1 for getCountryCount" in {


      countryMockDao.count() returns(Future[Int]{countries.length})

      val result = queryService.getCountryCount()
      val response = Await.result(result, Duration.Inf)
      response must_==(1)
    }
    "return count = 1 for getAirportCount" in {


      airportMockDao.count() returns(Future[Int]{airports.length})

      val result = queryService.getAirportCount()
      val response = Await.result(result, Duration.Inf)
      response must_==(1)
    }
    "return count = 1 for getRunwayCount" in {


      runwayMockDao.count() returns(Future[Int]{runways.length})
      val queryService = new QueryServiceImpl(countryMockDao, runwayMockDao, airportMockDao)

      val result = queryService.getRunwayCount()
      val response = Await.result(result, Duration.Inf)
      response must_==(1)
    }

  }
}
