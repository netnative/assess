package dao

import models.Country
import models.viewModels.{CountryView, SimpleCountryView}

import scala.concurrent.Future

trait CountryDao {

  def insert(countries:Seq[Country]): Future[Unit]
  def count():Future[Int]
  def all():Future[Seq[SimpleCountryView]]
  def getByName(name:String):Future[Seq[CountryView]]
  def getTopDownCountries(order:String, count:Int) : Future[Seq[SimpleCountryView]]
  def getCountriesSurface(count:Int):Future[Seq[(String, Seq[Option[String]])]]

}
