package dao

import models.Runway
import models.viewModels.SimpleRunWay

import scala.concurrent.Future

trait RunwayDao {
  def insert(runways:Seq[Runway]):Future[Unit]
  def count():Future[Int]
  def getByAirportIdent(ident:String):Future[Seq[SimpleRunWay]]
  def getMostCommonRunways(count:Int) : Future[Seq[Option[String]]]
}
