package controllers

import javax.inject._

import dao.{AirportDao, CountryDao, RunwayDao}
import play.api.libs.json.Json
import play.api.mvc._
import play.api.routing.JavaScriptReverseRouter
import services.QueryService

import scala.concurrent.{ExecutionContext, Future}

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(queryService: QueryService, cc: ControllerComponents)(implicit ec:ExecutionContext) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page with a welcome message.
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index = Action {

    Ok(views.html.index("Your new application is ready."))
  }

  /**
    * This returns the search HTML page
    * This will be return page with /country path
    * @return
    */
  def query = Action{implicit request =>
    Ok(views.html.query())
  }
  /**
    * This returns the search HTML page
    * This will be return page with /country path
    * @return
    */
  def report = Action{implicit request =>
    Ok(views.html.report())
  }

  /**
    * This method returns the countries along with threir airports
    * @param filter :It could be the country name in partial mode or country strict code
    * @return : Json of CountryView view model
    */
  def getCountries(filter:String) = Action.async{ implicit request =>
    queryService.getCountriesByName(filter).map(countries => Ok(Json.toJson(countries)))
    /*
    request.body.validate[QueryRequest].map{
      query => countryDao.getByName(query.filter).map(countries => Ok(Json.toJson(countries)))
    }.getOrElse(Future.successful(BadRequest("Invalid input value")))
    */
  }

  /**
    * Returns runways for the given airport ident
    * @param airportIdent
    * @return Json of runways
    */
  def getRunways(airportIdent:String) = Action.async{ implicit  request =>
    queryService.getRunwaysByAirportIden(airportIdent).map(runways => Ok(Json.toJson(runways)))
  }

  /**
    * Returns thecountirs with most and less airport counts
    * @param order : "asc" for countries with less airport counts, "desc" for countries with most airports count
    * @param count : the count of returned countries
    * @return country list in a json format
    */
  def getTopDownCountires(order:String, count:Int) = Action.async{implicit request =>
    queryService.getMostAndLeastCountries(order, count).map(countries => Ok(Json.toJson(countries)))
  }

  /**
    * Get a count and return the most runways base on the leIden field
    * @param count default value is set to 10
    * @return Array of Strings contains leIden
    */
  def getMostCommonIdent(count:Int) = Action.async{implicit request =>
    queryService.getMostCommonLeIdenInRunways(count).map(idens => Ok(Json.toJson(idens)))
  }

  /**
    * for each country its returns the runways surface of country airports
    * @param count : number of countris
    * @return : Json of string, seq[String] => country, Seq[Serface]
    */
  def getCityAirportRunwaySurface(count:Int) = Action.async{implicit request =>
    queryService.getSurfaceForEachCountry(count).map(res => Ok(Json.toJson(res)))
  }

  /**
    * this for ajax call
    * @return
    */
  def javascriptRoutes = Action { implicit request =>
    Ok(
      JavaScriptReverseRouter("jsRoutes")(
        routes.javascript.HomeController.getCountries,
        routes.javascript.HomeController.getRunways,
        routes.javascript.HomeController.getTopDownCountires,
        routes.javascript.HomeController.getCityAirportRunwaySurface,
        routes.javascript.HomeController.getMostCommonIdent
      )
    ).as("text/javascript")
  }
}
