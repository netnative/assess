package services

import models.viewModels.{CountryView, SimpleCountryView, SimpleRunWay}

import scala.concurrent.Future


trait QueryService {

  def getCountriesByName(filter:String):Future[Seq[CountryView]]

  def getRunwaysByAirportIden(airportIden:String):Future[Seq[SimpleRunWay]]
  def getMostAndLeastCountries(order:String, count:Int):Future[Seq[SimpleCountryView]]
  def getMostCommonLeIdenInRunways(count:Int):Future[Seq[Option[String]]]
  def getSurfaceForEachCountry(count:Int):Future[Seq[(String, Seq[Option[String]])]]
  def getCountryCount():Future[Int]
  def getRunwayCount():Future[Int]
  def getAirportCount():Future[Int]

  def insert[BaseModel](countries:Seq[BaseModel]):Future[Unit]
}
