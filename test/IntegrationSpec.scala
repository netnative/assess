import java.io.File
import java.util.concurrent.TimeUnit

import org.specs2.mutable.Specification
import play.api.test.Helpers.HTMLUNIT
import play.api.test.Helpers.running
import play.api.test.TestServer

class IntegrationSpec extends Specification {

  "Application" should {
    "work with in a browser" in {
      val port = 2233
      running(TestServer(port), HTMLUNIT) { browser =>
        browser.goTo("http://localhost:" + port)
        browser.$("head title").first.text() must equalTo("Welcome to Lunatech Assessment")


        browser.$("#btnQuery").first.click()
        browser.$("#txtfilter").first().value must equalTo("")

        browser.$("#txtfilter").first.write("iran")
        browser.$("#btnSearch").first.click()
        browser.await().atMost(10, TimeUnit.SECONDS).until(browser.el("#lblColumn")).present()

        browser.$("h2").first().text() must equalTo("Iran:AS")

        browser.$("#btnReport").first.click()
        browser.$("#btnReportShow").first.click()
        browser.await().atMost(10, TimeUnit.SECONDS).until(browser.el(".surfaceRows")).present()
        browser.$(".surfaceRows").count() must be_==(10)

       }
    }
  }
}
