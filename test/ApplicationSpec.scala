import play.api.Application
import play.api.test.{FakeRequest, PlaySpecification, WithApplication}


class ApplicationSpec  extends PlaySpecification{


  "Application" should {

    def HomeController(implicit app:Application)={
      val app2Controller = Application.instanceCache[controllers.HomeController]
      app2Controller(app)
    }

    "show main on /" in new WithApplication {
      val result = HomeController.index(FakeRequest())

      contentAsString(result) must contain("""<a id="btnQuery" class="btn btn-secondary" href="/query">Query</a>""")
    }

    "show report on /query" in new WithApplication{
      val result = HomeController.query(FakeRequest())
      contentAsString(result) must contain("""<input id="txtfilter" type="text" placeholder="Country partial name or code" aria-label="Country partial name or code" aria-describedby="basic-addon2" />""")
    }

    "show report on /report" in new WithApplication{
      val result = HomeController.report(FakeRequest())
      contentAsString(result) must contain("Countris with most airports")
    }

    "show NOT_FOUNT on /unknown address" in new WithApplication() {
      val result = route(app, FakeRequest(GET, "/test"))
      status(result.get) must equalTo(NOT_FOUND)
    }

    "getCountries with name equal iran" in new WithApplication{
      val result = HomeController.getCountries("iran")(FakeRequest())
      status(result) must equalTo(OK)
      contentAsString(result) must contain("Iran")
    }

    "getRunways for airportIden= 00A" in new WithApplication{
      val result = HomeController.getRunways("00A")(FakeRequest())

      status(result) must equalTo(OK)
      contentAsString(result) must contain(""""surface":"ASPH-G"""")
    }

  }
}
